class ApplicationMailer < ActionMailer::Base
  default from: 'hr@jobs-board.com'
  layout 'mailer'
end
